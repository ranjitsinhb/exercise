import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { City } from '../city';
import { CityService } from '../services/city-data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  details = {
    id: null,
    name: '',
    image: '',
    temperature: '',
    sunrise_time: '',
    sunset_time: '',
    sea_level: ''
  };
  cities = City;

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private _cityService: CityService

  ) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('id') !== 'null') {
      const id = parseInt(this.route.snapshot.paramMap.get('id'), 0);
      this.getCityDetails(id);
    }
  }

  showMap(lat: any, lng: any) {
    this.router.navigate(['/maps', lat, lng]);
  }

  getCityDetails(id) {
    this.details = this.cities.find(x => x.id === id);
    this._cityService.getCityDetailsByName(this.details).subscribe((data: any) => {
    });
  }


}
