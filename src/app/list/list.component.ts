import { Component, OnInit } from '@angular/core';
import { City } from '../city';
import { CityService } from '../services/city-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  cities = City;
  details = {
    id: null, 
    name: '',	
    image: '', 
    temperature: '', 
    sunrise_time:	'',
    sunset_time:	''
  };

  constructor(    
    private _cityService: CityService    
  ) {}

  ngOnInit() {    
    this.cities.forEach((city,index) => {
      this.getCityDetails(city.id, index);
    });
  }

  getCityDetails(id, index){
    this.details = this.cities.find(x => x.id === id);
    this._cityService.getCityDetailsByName(this.details).subscribe((data:any) => { 
        this.details[index] = data;
        return this.details;

    });
  }


}
