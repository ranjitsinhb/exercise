import { Injectable } from "@angular/core";
// import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CityService {

  apiURL = 'http://api.openweathermap.org';
  details = {
    id: null,
    name: '',
    image: '',
    temperature: '',
    sunrise_time: '',
    sunset_time: '',
    sea_level:''
  };

  constructor(
    private _http: HttpClient
  ) { }

  fetchCities() {
    return this._http.get(this.apiURL + '/data')
      .pipe(map(() => '')
      )
  }

  getCityDetailsByName(city: any) {
    return this._http.get(this.apiURL + '/data/2.5/weather?q=' + city.name + '&units=imperial&appid=3d8b309701a13f65b660fa2c64cdc517')
      .pipe(
        map((data: any) => {
          this.details = city;
          let sunsetTime = new Date(data.sys.sunset * 1000);
          let sunriseTime = new Date(data.sys.sunrise * 1000);
          this.details.sunset_time = sunsetTime.toLocaleTimeString();
          this.details.sunrise_time = sunriseTime.toLocaleTimeString();
          this.details.temperature = (data.main.temp).toFixed(1) + 'F';
          this.details.sea_level = data.main.pressure;
            return this.details;
        })
      )
  }

}