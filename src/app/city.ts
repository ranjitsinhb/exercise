export const City = [
  { 
    id: 1, 
    name: 'London',	
    image: 'assets/logos/10-London.jpg', 
    temperature: '', 
    sunrise_time:	'',
    sunset_time:	'',
    sea_level:''
  },
  { 
    id: 2, 
    name: 'Paris',	
    image: 'assets/logos/20-Paris.jpeg', 
    temperature: '', 
    sunrise_time:	'',
    sunset_time:	'',
    sea_level:''
  },
  { 
    id: 3, 
    name: 'Berlin',	
    image: 'assets/logos/30-Berlin.jpg', 
    temperature: '', 
    sunrise_time:	'',
    sunset_time:	'',
    sea_level:''
  },
  { 
    id: 4, 
    name: 'Lisbon',	
    image: 'assets/logos/40-Lisbon.jpeg', 
    temperature: '', 
    sunrise_time:	'',
    sunset_time:	'',
    sea_level:''
  },
  { 
    id: 5, 
    name: 'Amsterdam',	
    image: 'assets/logos/50-Amsterdam.jpg', 
    temperature: '', 
    sunrise_time:	'',
    sunset_time:	'',
    sea_level:''
  }
];
